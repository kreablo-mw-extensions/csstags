<?php
/**
 * Copyright (C) 2013 Andreas Jonsson <andreas.jonsson@kreablo.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Extensions
 */

class CssTagsHooks {

    public static function init( Parser $parser ) {
        global $egCssTags;

        foreach ($egCssTags as $tag => $css) {
            $parser->setHook($tag, function( $input, array $args, Parser $parser, $frame ) use ($tag, $css) {
                    self::insert( $parser, $tag, $css );

                    return '';
                });
        }

        return true;
    }

    private static function insert( Parser $parser, $tag, $css ) {
        $title = Title::newFromText( 'csstags-' . $tag , NS_MEDIAWIKI );
        $page = WikiPage::factory( $title );
        $output = $parser->getOutput();

        if ( $page->exists() ) {
            $csscode = htmlspecialchars( $page->getContent( Revision::RAW )->serialize() );
            // Treat the included CSS code as a template for proper cache handling.
            $output->addTemplate( $title, $page->getId(), $page->getRevision() );
        } else {
            $csscode = htmlspecialchars( $css );
        }

        $output->addHeadItem( '<style type="text/css">/*<![CDATA[*/'
                              . $csscode
                              . '/*]]>*/</style>' );
    }

}