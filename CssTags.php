<?php
/**
 * Copyright (C) 2013 Andreas Jonsson <andreas.jonsson@kreablo.se>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @file
 * @ingroup Extensions
 */

if ( !defined( 'MEDIAWIKI' ) ) {
	exit;
}

$extensionDir = dirname( __FILE__ );

$wgExtensionCredits['csstags'][] = array(
	'path' => __FILE__,
	'name' => 'CssTags',
	'author' => array( 'Andreas Jonsson' ),
	'url' => '',
	'version' => '1.0',
	'descriptionmsg' => 'csstags-desc',
);

if (!isset($egCssTags)) {
    $egCssTags = array();
}

$wgExtensionMessagesFiles['ToolboxSorter'] = "$extensionDir/CssTags.i18n.php";
$wgHooks['ParserFirstCallInit'][] = 'CssTagsHooks::init';
$wgAutoloadClasses['CssTagsHooks'] = $extensionDir . '/CssTagsHooks.php';

