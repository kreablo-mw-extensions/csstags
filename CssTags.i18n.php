<?php
/**
 * Internationalisation file for extension CssTags
 *
 * @file
 * @ingroup Extensions
 */

$messages = array();

$messages['en'] = array(
    'csstags' => 'CSS Tags',
    'csstags-desc' => 'Adds a configurable set of xml tags for inserting CSS code on html pages',
);